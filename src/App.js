import React, { Component } from "react";
import StudentRegisterForm from "./pages/Student/StudentRegisterForm/StudentRegisterForm";
import StudentProfile from "./pages/Student/StudentProfile/StudentProfile";

import TeacherRegisterForm from "./pages/Teacher/TeacherRegisterForm";
import InstituteRegister from "./pages/Institute/InstituteRegister/InstituteRegister";
import { BrowserRouter as Router, Route } from "react-router-dom";

function App() {
  return (
    <Router>
      <Route path="/" exact component={StudentRegisterForm} />
      <Route path="/StudentProfile" component={StudentProfile} />
      <Route path="/TeacherRegisterForm" component={TeacherRegisterForm} />
      <Route path="/InstituteRegister" component={InstituteRegister} />
    </Router>
  );
}

export default App;
