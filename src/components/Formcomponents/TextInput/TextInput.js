import React, { Component } from "react";
// import { Form, Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import {
  FormControl,
  InputLabel,
  Input,
  FormHelperText,
  TextField,
} from "@material-ui/core";
export default class TextInput extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <>
        <div className="form-group" style={{ marginTop: "25px" }}>
          <FormControl fullWidth={true}>
            <TextField
              id="standard-basic"
              label={this.props?.label}
              value={this.props.value}
              onChange={(e) => this.props.onChange(e.target.value)}
              placeholder={this.props.placeholder}
            />
            {/* <Input onChange={(e) => this.props.onChange(e.target.value)} id="my-input" aria-describedby="my-helper-text" /> */}
            <FormHelperText id="my-helper-text">
              {this.props.helperText}
            </FormHelperText>
          </FormControl>
        </div>
      </>
    );
  }
}
// onChange={(e) => this.props.onchange(e.target.value)}
