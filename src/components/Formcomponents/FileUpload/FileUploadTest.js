import React, { Component } from 'react'
import {DropzoneDialog} from 'material-ui-dropzone'
import Button from '@material-ui/core/Button';

export default class FileUploadTest extends Component {
    constructor(props){
        super(props);
        this.state = {
            isOpen:true
          };
    }
    render() {
        return (
            <div>
            <Button onClick={()=>this.setState({isOpen:true})}>
              Add Image
            </Button>
            <DropzoneDialog
                open={this.state.isOpen}
                // onSave={this.handleSave.bind(this)}
                acceptedFiles={['image/jpeg', 'video/mp4', 'image/bmp']}
                showPreviews={true}
                maxFileSize={5000000}
                onClose={()=>this.setState({isOpen:false})}
            />
        </div>
        )
    }
}
