import React, { Component } from "react";
import {
  FormControl,
  InputLabel,
  Input,
  FormHelperText,
  TextField,
} from "@material-ui/core";
export default class TextArea extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <>
        <div className="form-group" style={{ marginTop: "25px" }}>
          {/* <label for="exampleFormControlTextarea1">{this.props.label}</label>
        <textarea
          onChange={(e) => this.props.onChange(e.target.value)}
          placeholder={this.props.placeholder}
          className="form-control"
          id="exampleFormControlTextarea1"
          rows={this.props.rows}
        ></textarea> */}
          <FormControl fullWidth={true}>
            <TextField
              onChange={(e) => this.props.onChange(e.target.value)}
              id="standard-textarea"
              label={this.props.label}
              value={this.props.value}
              placeholder={this.props.placeholder}
              multiline
            />
          </FormControl>
        </div>
      </>
    );
  }
}
