import React, { Component } from "react";
import { FormControl,InputLabel,Input,FormHelperText,TextField,Select,MenuItem } from '@material-ui/core';
export default class SelectBox extends Component {
    constructor(props){
        super(props);
        this.state={
            age:0,
            array:[
                {state:"kerala",value:"kerala"},
                {state:"tamilnadu",value:"tamilnadu"},
                {state:"karnataka",value:"karnataka"},
            ]
        }
    }
  render() {
      
    return (
      <div className="form-group" style={{ marginTop: "25px" }}>
        <FormControl fullWidth={true} >
          <InputLabel id="demo-simple-select-helper-label">{this.props.label}</InputLabel>
          <Select
            placeholder={this.props.placeholder}
            labelId="demo-simple-select-helper-label"
            id="demo-simple-select"
            value={this.state.value}
            onChange={(e)=>{
              this.setState({value:e.target.value})
              this.props.onChange(e.target.value)}}
          >
              {this.state.array.map(item =>
            <MenuItem value={item.value}>{item.state}</MenuItem>
              )}
          </Select>
          <FormHelperText style={{color:this.props.suportercolor}}>{this.props.supportText}</FormHelperText>
        </FormControl>
      </div>
    );
  }
}
