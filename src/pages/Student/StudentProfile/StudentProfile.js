import React, { Component } from "react";
import { Container, Row, Col } from "react-bootstrap";
import TextInput from "../../../components/Formcomponents/TextInput";
import TextArea from "../../../components/Formcomponents/TextArea";
import SelectBox from "../../../components/Formcomponents/SelectBox";
import {
  FormControl,
  InputLabel,
  Input,
  FormHelperText,
  TextField,
  Box,
  Typography,
} from "@material-ui/core";
import { shadows } from "@material-ui/system";
import DatePickerSelect from "../../../components/Formcomponents/DatePicker";
export default class StudentProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      windowWidth: window.innerWidth,
      windowHeight: window.innerHeight,
      studentname: "",
      address: "",
      email: "",
      date: "",
      states: "",
      names: [],
    };
  }
  handleResize = (e) => {
    this.setState({
      windowWidth: window.innerWidth,
      windowHeight: window.innerHeight,
    });
  };
  componentDidMount() {
    window.addEventListener("resize", this.handleResize);
  }
  componentWillUnmount() {
    window.addEventListener("resize", this.handleResize);
  }
  fileupload = () => {
    // alert(this.state.date);
    fetch("http://192.168.43.125:4000/api/student/get-student", {
      // fetch("http://localhost:4000/api/student/add-student", {
      // mode: "no-cors",
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        student_id: 2,
        // name: this.state.studentname,
        // age: 4,
        // address: this.state.address,
        // state: this.state.states,
        // email: this.state.email,
        // dob: this.state.date,
      }),
    })
      .then((response) => response.json())
      .then((json) => {
        console.log(JSON.stringify(json));
        if (json) {
          console.log("hi");
          //   alert("submited");
          this.setState({
            studentname: json.data.name,
            age: 4,
            address: json.data.address,
            states: json.data.state,
            email: json.data.email,
            date: json.data.dob,
          });
          console.log(json.data.name, "popopoop");

          // this.props.navigation.goBack();
        } else {
          alert(json.message);
          console.log(json.message);
        }
      })
      .catch((error) => console.error(error));
    //   .finally(() => this.setState({ isloading: false }));
  };
  componentDidMount() {
    this.fileupload();
  }
  render() {
    console.log(this.state.studentname, "got it");
    return (
      <>
        {/* <div className="col-12" style={styles.headreDiv}>
          <div className="col-4" style={{ backgroundColor: "red" }}></div>
          <div className="col-4" style={{ backgroundColor: "green" }}></div>
          <div className="col-4" style={{ backgroundColor: "yellow" }}></div>
        </div> */}
        <div style={{ marginTop: "20px" }}>
          <Box
            style={{
              width: "90%",
              margin: "0 auto",
              backgroundColor: "white",
            }}
            boxShadow={3}
          >
            <Typography
              style={{ width: "95%", margin: "0 auto", paddingTop: "20px" }}
              variant="h4"
              component="h2"
            >
              Student Profile
            </Typography>
            <Container style={{ padding: 10, backgroundColor: "white" }}>
              <Row xs={1} lg={2} style={{ backgroundColor: "" }}>
                <Col>
                  <form>
                    <TextInput
                      placeholder={"Institute Name"}
                      type={"text"}
                      label={"Student name"}
                      value={this.state.studentname}
                      onChange={(value) =>
                        this.setState({
                          studentname: value,
                        })
                      }
                    />
                    <TextArea
                      label={" Address"}
                      value={this.state.address}
                      placeholder={"student address"}
                      onChange={(value) =>
                        this.setState({
                          address: value,
                        })
                      }
                    />
                    <SelectBox
                      label={"State"}
                      placeholder={"select state"}
                      value={this.state.states}
                      onChange={(value) =>
                        this.setState({
                          states: value,
                        })
                      }
                    />
                  </form>
                </Col>
                <Col style={{ marginLe: "20px" }} lg="6">
                  <TextInput
                    placeholder={"www.sample.com"}
                    type={"text"}
                    value={this.state.email}
                    label={"Email Address"}
                    onChange={(value) =>
                      this.setState({
                        email: value,
                      })
                    }
                  />
                  <DatePickerSelect
                    label={"Dte of Birth"}
                    value={this.state.date}
                    onChange={(value) =>
                      this.setState({
                        date: value,
                      })
                    }
                  />
                </Col>
              </Row>
              <Row>
                <Col></Col>
                <Col></Col>
                <Col>
                  {/* <div
                    style={{ marginTop: "10px" }}
                    type="submit"
                    className="btn btn-primary"
                    onClick={() => this.fileupload()}
                  >
                    Submit
                  </div> */}
                </Col>
              </Row>
            </Container>
          </Box>
        </div>
      </>
    );
  }
}

const styles = {
  headreDiv: {
    width: "100%",
    height: "80px",
    backgroundColor: "lightblue",
    flexDirection: "row",
    display: "flex",
    justifyContent: "center",
  },
  mainDiv: {
    backgroundColor: "blue",
    flexDirection: "row",
    justifyContent: "center",
    display: "flex",
  },
  well: {
    webkitBoxShadow: "1px 3px 1px #9E9E9E",
    mozBoxShadow: "1px 3px 1px #9E9E9E",
    boxShadow: "1px 3px 1px #9E9E9E",
  },
};
