import React, { Component } from "react";
import { Container, Row, Col } from "react-bootstrap";
import TextInput from "../../../src/components/Formcomponents/TextInput";
import TextArea from "../../../src/components/Formcomponents/TextArea";
import SelectBox from "../../../src/components/Formcomponents/SelectBox";
import {
  FormControl,
  InputLabel,
  Input,
  FormHelperText,
  TextField,
  Box,
  Typography,
} from "@material-ui/core";
import { shadows } from "@material-ui/system";
import DatePickerSelect from "../../../src/components/Formcomponents/DatePicker/DatePicker";
export default class TeacherRegisterForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      windowWidth: window.innerWidth,
      windowHeight: window.innerHeight,
    };
  }
  handleResize = (e) => {
    this.setState({
      windowWidth: window.innerWidth,
      windowHeight: window.innerHeight,
    });
  };
  componentDidMount() {
    window.addEventListener("resize", this.handleResize);
  }
  componentWillUnmount() {
    window.addEventListener("resize", this.handleResize);
  }
  render() {
    return (
      <>
        {/* <div className="col-12" style={styles.headreDiv}>
          <div className="col-4" style={{ backgroundColor: "red" }}></div>
          <div className="col-4" style={{ backgroundColor: "green" }}></div>
          <div className="col-4" style={{ backgroundColor: "yellow" }}></div>
        </div> */}
        <div style={{ marginTop: "20px" }}>
          <Box
            style={{
              width: "90%",
              margin: "0 auto",
              backgroundColor: "white",
            }}
            boxShadow={3}
          >
            <Typography
              style={{ width: "95%", margin: "0 auto", paddingTop: "20px" }}
              variant="h4"
              component="h2"
            >
              Teacher Register Form
            </Typography>
            <Container style={{ padding: 10, backgroundColor: "white" }}>
              <Row xs={1} lg={2} style={{ backgroundColor: "" }}>
                <Col>
                  <form>
                    <TextInput
                      placeholder={"Institute Name"}
                      type={"text"}
                      label={"Teacher name"}
                      onChange={(value) => console.log(value)}
                    />
                    <TextArea
                      label={" Address"}
                      placeholder={"Institute address"}
                      onChange={(value) => console.log(value)}
                    />
                    <SelectBox label={"State"} placeholder={"select state"} />
                  </form>
                </Col>
                <Col style={{ marginLe: "20px" }} lg="6">
                  <TextInput
                    placeholder={"www.sample.com"}
                    type={"text"}
                    label={"Email Address"}
                    onChange={(value) => console.log(value)}
                  />
                  <DatePickerSelect label={"Dte of Birth"} />
                </Col>
              </Row>
              <Row>
                <Col></Col>
                <Col></Col>
                <Col>
                  <div
                    style={{ marginTop: "10px" }}
                    type="submit"
                    className="btn btn-primary"
                  >
                    Submit
                  </div>
                </Col>
              </Row>
            </Container>
          </Box>
        </div>
      </>
    );
  }
}

const styles = {
  headreDiv: {
    width: "100%",
    height: "80px",
    backgroundColor: "lightblue",
    flexDirection: "row",
    display: "flex",
    justifyContent: "center",
  },
  mainDiv: {
    backgroundColor: "blue",
    flexDirection: "row",
    justifyContent: "center",
    display: "flex",
  },
  well: {
    webkitBoxShadow: "1px 3px 1px #9E9E9E",
    mozBoxShadow: "1px 3px 1px #9E9E9E",
    boxShadow: "1px 3px 1px #9E9E9E",
  },
};
